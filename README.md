# README #

This README provides details and guidelines pertaining to the ELEN E4750 project.

### What is this repository for? ###

* In it's current state, this code will run on an Arrow SoCKit; however, the device code is not properly configured and is considered in a debug state. (TODO: Update)
* Initial debug release v0.1

### Contribution guidelines ###

* If you wish to contribute, please contact me. Without consenet, 
please do not contribute to this repo. This is considered a privately 
maintained project and is public only for grading.

### Who do I talk to? ###
Nicholas Christman
EE Master's Student, Columbia University