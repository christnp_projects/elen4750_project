// OpenCL Image Filter Kernel
// in = original image of out_m-by-out_n
// fmask = image filter mask (kernel) of mask_dim-by-mask_dim
// out = filtered image of size out_m-by-out_n
// pitch = used for padding of image based on mask dim
//__attribute__((max_work_group_size(128*128)))
__kernel void imfilter( 	__global const uchar *restrict in,
							__constant char * fmask,
							__global uchar *restrict out,
							uint out_m,
							uint out_n,
							uint mask_dim,
							uint pitch) 
{

	// START OF KERNEL
	
	// Get the global ID of the work item
	int out_row = get_global_id(0); // Current Row
    int out_col = get_global_id(1); // Current Col
    
    // Define bounds for output
	int out_n2 = get_global_size(0);	// Output max Rows (height)
	int out_m2 = get_global_size(1); // Output max Cols (width)

	// Current thread location
	int cur_loc = out_row*out_m+out_col;

	// Private variables
	int cur_input_start = 0, cur_input_loc = 0; // Input matrix start and current locations
	int cur_mask_row = 0,cur_mask_col = 0,cur_mask_loc = 0; // Mask matrix current locations
	int temp = 0; // Temporary output storage

	// Convolve the filter over the input	
	for(int mask_row=0;mask_row<mask_dim;mask_row++){		
		
		cur_mask_row = (cur_loc+mask_row)*mask_dim; // Where I a now plus where I am in the filter
		cur_input_start = out_row*pitch + out_col;

		//#pragma unroll // directive to unroll the inner-loop per Altera's guidlines (pg. 1-12)
		for(int mask_col=0;mask_col<mask_dim;mask_col++){	
			
			cur_input_loc = cur_input_start + mask_row*pitch + mask_col;
			cur_mask_loc = mask_row*mask_dim + mask_col;

			temp += in[cur_input_loc] * fmask[cur_mask_loc];
		}
	}	
	
	//barrier(CLK_LOCAL_MEM_FENCE);	
	//out[cur_loc] = temp; // Put the temp var into the buffer 
	if(cur_loc == 0)
		out[0] = 99;
	if(cur_loc == 1)
		out[1] = out_m;
	if(cur_loc == 2)
		out[2] = mask_dim;
	if(cur_loc == 3)
		out[3] = pitch;
	else		
		out[cur_loc] = in[cur_loc]; // Put the temp var into the buffer 		
	
	//out[1] = out_m;
	//out[2] = out_n;
	//out[3] = out_m2;
	//out[4] = out_n2;
	//out[5] = mask_dim;
	//out[6] = pitch;
	
	
	// END OF KERNEL
}

