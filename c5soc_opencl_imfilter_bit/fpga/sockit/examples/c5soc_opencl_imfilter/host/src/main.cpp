/********************************** FILE HEADER ***********************************************
* File:.........main.cpp
* Editor:.......Nicholas Christman
* Version:......1.0
* 
* Description:..This host program runs a image filter kernel (imfilter). The imfilter kernel
*               takes an image, filters the image using the defined mask, and outputs the 
*               filtered image. Important: this code recognizes jpeg images only!
* 
* Usage:........This main() function and helper functions were written for use on the Arrow 
*               Cyclone V SoCKit. The shell of this file was provided as example code with 
*               Altera tools (see the legal notice below). In addtion, the LodePNG C/C++ library 
*               is used to read in and write to stnadard PNG files (see lodepng.h)
* 
* Change Log:...Hisotry of important changes in chronological order
*
*         20161127 - init 
*         20161209 - aocl not working for Cyclone V SoCKit; changing Makefile to point local
*         20161210 - libjpeg libraries built and integrated, but not working
*         20161216 - unresolved support by Terasic temporarily fixed by using the incorrect
*                     aocl version to init_opencl on DevKit and the new version to program
*         20161217 - portable lodepng library used instead of static libjpeg library
*
**********************************************************************************************/

/********************************** LEGAL NOTICES ********************************************
// Copyright (C) 2013-2014 Altera Corporation, San Jose, California, USA. All rights reserved. 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this 
// software and associated documentation files (the "Software"), to deal in the Software 
// without restriction, including without limitation the rights to use, copy, modify, merge, 
// publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to 
// whom the Software is furnished to do so, subject to the following conditions: 
// The above copyright notice and this permission notice shall be included in all copies or 
// substantial portions of the Software. 
//  
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
// OTHER DEALINGS IN THE SOFTWARE. 
//  
// This agreement shall be governed in all respects by the laws of the State of California and 
// by the laws of the United States of America. 
**********************************************************************************************/
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cstring>
#include <iterator>
#include <vector>
#include "CL/opencl.h"
#include "AOCL_Utils.h"
#include "lodepng.h"

using namespace aocl_utils;


/************* Define static names **************/
#define CL_KERNEL1_FILE "device/imfilter.cl"
#define CL_KERNEL1_NAME "imfilter"

#define MAX_IMAGE_SIZE (2048*2048) // Adjust this if larger images are needed; this is for temp_image
#define CL_IMAGE_IN "image1-gs.png" // Grayscale PNG input image
#define CL_IMAGE_OUT_CPU "cpu-output1-gs.png" // Grayscale output image from CPU
#define CL_IMAGE_OUT_FPGA "gpu-output1-gs.png" // Grayscale output image from CPU

#define STRING_BUFFER_LEN 1024

// Use for Altera recommended 64-byte buffer alignment
#define AOCL_ALIGNMENT 64
void *ptr = NULL;


/****** Define basic filters mask/kernel *******/
// LOG = 3x3 Laplacian of Gaussian
#define LOG 1
// BLUR = 5x5 Blurring filter
#define BLUR 2

// Choose filter
#define FMASK 0

#if FMASK == LOG
#define MASK_DIM 3
static int8_t fmask_buf[MASK_DIM][MASK_DIM] =
{
  -1, -1, -1,
  -1,  8, -1,
  -1, -1, -1
};

#elif FMASK == BLUR 
#define MASK_DIM 5
static int8_t fmask_buf[MASK_DIM][MASK_DIM] =
{
  0, 0, 1, 0, 0,
  0, 1, 1, 1, 0,
  1, 1, 1, 1, 1,
  0, 1, 1, 1, 0,
  0, 0, 1, 0, 0,
};

#else // Identity
#define MASK_DIM 3
static int8_t fmask_buf[MASK_DIM][MASK_DIM] =
{
  0, 1, 0,
  0, 1, 0,
  0, 0, 0
};
#endif

#define PITCH floor(MASK_DIM/2)
/**********************************************/

/***** Define image and filter variables ******/
uint8_t* cpu_output_buf;    // Results buffer from CPU process
uint8_t* ocl_output_buf;    // Results buffer from FPGA process
uint8_t* temp_img_buf;      // Temporary image buffer
uint8_t* input_img_buf;     // Input image buffer
unsigned imWidth, imHeight; // Input image dimensions

static const size_t imChannels = 1; // Assume Grayscale (vs. RGB = 3)

size_t imBytes; // Number of elements/bytes in the image
size_t imPixels; // Number of pixels in the image
size_t fmElements; // Number of elements in the filter mask

// OpenCL Kernel Runtime constants
// Used to define the work set over which this kernel will execute.
// Let's make the global size (total WIs) be 512x512 and teh local (WI/WG) size be 32
static const size_t gSizex = 512/32;//512; 
static const size_t gSizey = 1;//512;  
static const size_t wgSizex = 1;//32; 
static const size_t wgSizey = 1; 

// OpenCL runtime configuration
static cl_platform_id platform = NULL;
static cl_device_id device = NULL;
static cl_context context = NULL;
static cl_command_queue queue = NULL;
static cl_kernel kernel = NULL;
static cl_program program = NULL;
static cl_mem ocl_bufIn = 0;
static cl_mem ocl_bufMask = 0;
static cl_mem ocl_bufOut = 0;

// Used for buffer mapping
uint8_t* input_img_map_buf;
int8_t* fmask_map_buf;
uint8_t* ocl_output_map_buf;

// Used for source kernel string
char *kernel_str; 
size_t kernel_str_size;

// Error tolerances
float etol = 0.01;  // As decimal (i.e., .01 = 1%)


// Function prototypes
bool initOpenCL();
void initBuffer(uint8_t* buf,uint32_t buf_size,uint8_t buf_val);
void cpuConvolution();
int readKernelSource(const char* filename);
void cleanup();

static void device_info_ulong( cl_device_id device, cl_device_info param, const char* name);
static void device_info_uint( cl_device_id device, cl_device_info param, const char* name);
static void device_info_bool( cl_device_id device, cl_device_info param, const char* name);
static void device_info_string( cl_device_id device, cl_device_info param, const char* name);
static void display_device_info( cl_device_id device );

void decodePNG(const char* filename);
void encodePNG(const char* filename, const unsigned char* image, unsigned imWidth, unsigned imHeight);

// Entry point.
int main() {
  cl_int status;

  // Allocate memory for initial image read; this will be transfered and aligned later
  temp_img_buf = (uint8_t *)malloc(MAX_IMAGE_SIZE * sizeof(temp_img_buf)*4);

  // Variables for timing
  double start_time = 0;
  double end_time = 0;
  double gpu_time,cpu_time;
  
  /************ Load PNG image into input_img_buf ************/
  decodePNG((const char *) CL_IMAGE_IN);

  //////// DEBUG INPUT ///////
  // Initialize the temp buffer to be 64 values of 1
  //initBuffer(temp_img_buf,8*8,1);
  //imWidth = 8;
  //imHeight = 8;
  ///////////////////////////

  // Total number of pixels in the input image
  imPixels = imWidth*imHeight*imChannels;
  imBytes = imWidth*imHeight*imChannels*4; // *4 is necessary for PNG encode/decode

  // Allocate image memory alligned to AOCL_ALIGNMENT (Altera recommends 64-byte for DMA'ing)
  status = posix_memalign ((void **)&input_img_buf, AOCL_ALIGNMENT, imBytes*sizeof(uint8_t));
  if(status != 0) {
    fprintf(stderr, "%s\n","Failed posix_memalign" );
    return 1;
  }
  // Copy image data from temp_img_buf to input_img_buf
  memcpy(input_img_buf,temp_img_buf,imBytes*sizeof(uint8_t));
  /***********************************************************/

  //////// DEBUG INPUT ///////
  //printf("Input:\n");
  //for(int i=0;i<imHeight*imWidth;i++)
  //  printf(" %d ",input_img_buf[i]);
  ///////////////////////////
  
  /************* Define the image and mask variables *************/
  uint pitch = (uint) PITCH;
  uint mask_dim = (uint) MASK_DIM;
  fmElements = mask_dim*mask_dim;
  

  /*********** Allocate input and output image buffers ************/
  status = posix_memalign ((void **)&cpu_output_buf, AOCL_ALIGNMENT, imBytes*sizeof(uint8_t));
  if(status != 0) {
    fprintf(stderr, "%s\n","Failed posix_memalign" );
    return 1;
  }

  status = posix_memalign ((void **)&ocl_output_buf, AOCL_ALIGNMENT, imBytes*sizeof(uint8_t));
  if(status != 0) {
    fprintf(stderr, "%s\n","Failed posix_memalign" );
    return 1;
  } 
  // Initialize local (cpu) output buffers
  for(int i=0; i < imBytes; i++){
    ocl_output_buf[i] = 64; 
    cpu_output_buf[i] = 0;  
  }  
  /****************************************************************/

  /************************* OPENCL Init **************************/ 
  status = initOpenCL();
  if(!status) {
    printf("\nInitializing OpenCL Kernel: \n");
    printf("==========================\n");
    printf("%-40s = %s\n\n", "KERNEL INIT", "FAILED");
    return -1;
  } 
  else{    
    printf("\nInitializing OpenCL Kernel: \n");
    printf("==========================\n");    
  } 

  // Set the kernel arguments
  // Input image argument (argument 0)
  status = clSetKernelArg(kernel, 0, sizeof(cl_mem), &ocl_bufIn); // Input image
  checkError(status, "Failed to set kernel arg 0");
  // Input mask argument (argument 1)
  status = clSetKernelArg(kernel, 1, sizeof(cl_mem), &ocl_bufMask); // Filter mask
  checkError(status, "Failed to set kernel arg 1");
  // Output image argument (argument 2)
  status = clSetKernelArg(kernel, 2, sizeof(cl_mem), &ocl_bufOut); // Output image
  checkError(status, "Failed to set kernel arg 2");
  // Input variable mask dim (argument 3)
  status = clSetKernelArg(kernel, 3, sizeof(uint), &imWidth); // Output image
  checkError(status, "Failed to set kernel arg 3");
  // Input variable mask dim (argument 3)
  status = clSetKernelArg(kernel, 4, sizeof(uint), &imHeight); // Output image
  checkError(status, "Failed to set kernel arg 4");
  // Input variable mask dim (argument 5)
  status = clSetKernelArg(kernel, 5, sizeof(uint), &mask_dim); // Output image
  checkError(status, "Failed to set kernel arg 5");
  // Input variable mask dim (argument 6)
  status = clSetKernelArg(kernel, 6, sizeof(uint), &pitch); // Output image
  checkError(status, "Failed to set kernel arg 6");

  // Configure work set over which the kernel will execute (defined globally above)
  size_t wgSize[3] = {wgSizex, wgSizey, 1};
  size_t gSize[3] = {gSizex, gSizey, 1};

  printf("%-40s = %s\n\n", "KERNEL INIT", "SUCCESS");
  /****************************************************************/

  /******************** Execute Kernel & Serial *******************/
  printf("\nBegin Image Processing: \n");
  printf("=========================\n");
  // Launch the FPGA Kernel //
  start_time = getCurrentTimestamp();// Start the counter  
  
  status = clEnqueueNDRangeKernel(queue, kernel, 2, NULL, gSize, wgSize, 0, NULL, NULL);
  checkError(status, "Failed to launch kernel");  
  // Wait for command queue to complete pending events
  status = clFinish(queue);
  checkError(status, "Failed to finish");  
  
  end_time = getCurrentTimestamp(); // Stop the counter

  gpu_time = (end_time - start_time);
  printf("%-40s = %s\n", "FPGA/OPENCL PROCESSING", "SUCCESS");
  //////////////////////////  
  
  // Launch the CPU Code //  
  start_time = getCurrentTimestamp(); // Start the counter

  cpuConvolution();

  end_time = getCurrentTimestamp(); // Stop the counter

  cpu_time = (end_time - start_time);
  printf("%-40s = %s\n\n", "CPU/SERIAL PROCESSING", "SUCCESS");
  //////////////////////////

  /****** Read data from ocl_output_buf into host address space *******/
  // Read output data from host buffer
  // for(int i=0;i<imBytes;i++) ocl_output_buf[i] = ocl_output_map_buf[i];
  // Unmap ocl_output_buf
  // clEnqueueUnmapMemObject(queue, ocl_bufOut, ocl_output_map_buf, 0, NULL, NULL);
  // printf("Unmapping ocl_output_map_buf complete!\n"); 
  /******************************************************************/ 

   clEnqueueReadBuffer(queue,ocl_bufOut,1,0,sizeof(ocl_output_buf),ocl_output_buf,0,0,0); 

  // Calculate the number of non-equal elements (compare CPU-to-GPU)
  int num_neqs = 0;
  for (int i = 0; i < imBytes; i++ ) {
    if ( cpu_output_buf[i] != ocl_output_buf[i] ) {  
      num_neqs++;
    }
  }

  const char* result = (num_neqs < etol*(float)imBytes) ? "TRUE" : "FALSE";

  printf("Execution Results: \n");
  printf("=========================\n");
  printf("%-40s = %s (+/-%0.2f%%)\n", "FPGA = CPU?", result,etol*100);
  printf("%-40s = %0.3f ms\n", "FPGA/OPENCL TIME", gpu_time * 1e3);
  printf("%-40s = %0.3f ms\n\n", "CPU/SERIAL TIME", cpu_time * 1e3);
  /****************************************************************/

  /****************** Encode the results as a PNG ******************/
  // TODO: This encodes the images as color; how to keep them grayscale?
  // Encode the CPU results as a PNG
  encodePNG((const char *) CL_IMAGE_OUT_CPU, cpu_output_buf, imWidth, imHeight);
  // Encode the FPGA results as a PNG
  encodePNG((const char *) CL_IMAGE_OUT_FPGA, ocl_output_buf, imWidth, imHeight);  
  /****************************************************************/

  /******************** DEBUG REMOVE BEFORE FINAL ******************/
  /*
  printf("\nSerial:\n");
  for(int i=0;i<imHeight;i++){
    for(int j=0;j<imWidth;j++)
       printf(" %d ",cpu_output_buf[i*imWidth + j]);
    printf("\n");
  }
  */
   
  printf("\nOpenCL:\n");
  for(int j=0;j<imWidth;j++)
    printf(" %d ",ocl_output_buf[j]);
  
  /****************************************************************/

  /******************** Do some cleanup and exit ******************/
  // Cleanup
  cleanup();

  // Exit main()
  printf("\nProject Complete!\n");
  return 0;
}


/*************************************************************************
 ************************ OPENCL INITIALIZATION **************************
 *************************************************************************/
bool initOpenCL() {
  cl_int status;

  if(!setCwdToExeDir()) {
    return false;
  }

  // Get the OpenCL platform.
  platform = findPlatform("Altera");
  if(platform == NULL) {
    printf("WARNING: Unable to find Altera OpenCL platform. Looking for Nvidia platform...\n");

    // Also check for Nvidia for Tesseract servers
    platform = findPlatform("NVIDIA CUDA");
    if(platform == NULL) {
      printf("ERROR: Unable to find Nvidia platform, exiting...\n");
      return false;
    }
  }

  char platform_name[STRING_BUFFER_LEN];
  clGetPlatformInfo(platform, CL_PLATFORM_NAME, STRING_BUFFER_LEN, platform_name, NULL);

  // User-visible ocl_output - Platform information
  {
    char char_buffer[STRING_BUFFER_LEN]; 
    printf("Querying platform for info:\n");
    printf("==========================\n");
    //clGetPlatformInfo(platform, CL_PLATFORM_NAME, STRING_BUFFER_LEN, char_buffer, NULL);
    printf("%-40s = %s\n", "CL_PLATFORM_NAME", platform_name);
    clGetPlatformInfo(platform, CL_PLATFORM_VENDOR, STRING_BUFFER_LEN, char_buffer, NULL);
    printf("%-40s = %s\n", "CL_PLATFORM_VENDOR ", char_buffer);
    clGetPlatformInfo(platform, CL_PLATFORM_VERSION, STRING_BUFFER_LEN, char_buffer, NULL);
    printf("%-40s = %s\n\n", "CL_PLATFORM_VERSION ", char_buffer);
  }

  // Query the available OpenCL devices.
  scoped_array<cl_device_id> devices;
  cl_uint num_devices;

  devices.reset(getDevices(platform, CL_DEVICE_TYPE_ALL, &num_devices));

  // We'll just use the first device.
  device = devices[0];

  // Display some device information.
  display_device_info(device);

  // Create the context.
  context = clCreateContext(NULL, 1, &device, NULL, NULL, &status);
  checkError(status, "Failed to create context");

  // Create the command queue.
  queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &status);
  checkError(status, "Failed to create command queue");

  // Create the program from Altera AOCX Device, else use standard Nvidia OpenCL.
  if(findPlatform("Altera")){
    std::string binary_file = getBoardBinaryFile(CL_KERNEL1_NAME, device);
    printf("%-40s = %s\n\n", "AOCX file being used", binary_file.c_str());
    program = createProgramFromBinary(context, binary_file.c_str(), &device, 1);
  }
  else{
    printf("%-40s = %s\n\n", "AOCX file being used", "N/A");
    // Load kernel into file pointer, save as source string
    if(!readKernelSource((const char*)CL_KERNEL1_FILE)){
      printf("\nWARNING: %s not read properly. clBuildProgram() might fail.\n",CL_KERNEL1_FILE);
    }
    kernel_str[kernel_str_size] = '\0'; // End binary file with a '\0'
    program = clCreateProgramWithSource(context, 1, (const char **)&kernel_str,
                                        (const size_t *)&kernel_str_size, &status);
  }


  // Build the program that was just created.
  static const char *compileOptions = "-D MASK_DIM=3 -D PITCH=1";

  status = clBuildProgram(program, 1, &device, compileOptions, NULL, NULL);
 
  // Print out compiler message if there is an error
  if (status == CL_BUILD_PROGRAM_FAILURE) {
    // Determine the size of the log
    size_t log_size;
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

    // Allocate memory for the log
    char *log = (char *) malloc(log_size);

    // Get the log
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

    // Print the log
    printf("%s\n", log);
  }

  // TODO:  I tried to optimize the FPGA OpenCL by utilizing CL_MEM_ALLOC_HOST_PTR; however,
  //        my buffers were not initializing write and it didn't seem to fix my errors. The
  //        "TODO" here is to fix this in the next revision.
  /***************** Prepare OpenCL memory objects ********************/
  // Use host ptr mapping instead of enquewrite
  /*ocl_bufIn = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, imBytes*sizeof(input_img_buf), NULL, &status);
  ocl_bufMask = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, fmElements*sizeof(fmask_buf), NULL, &status);
  ocl_bufOut = clCreateBuffer(context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, imBytes*sizeof(ocl_output_buf), NULL, &status);
  if(!ocl_bufIn || !ocl_bufMask || !ocl_bufOut){
    fprintf(stderr, "%s\n", "Failed to create buffers.");
  }*/
  /******************************************************************/

  /*********** Map input_img_buf into host address space ************/
  // input_img_map_buf = (uint8_t *)clEnqueueMapBuffer(queue, ocl_bufIn, CL_TRUE, CL_MAP_WRITE /*CL_MAP_READ*/, 0, imBytes*sizeof(input_img_buf), 0, NULL, NULL, &status);
  // checkError(status, "Failed to map input_img_map_buf buffer");
  // // Write image data into host buffer
  // for(int i=0;i<imBytes;i++) input_img_map_buf[i] = input_img_buf[i];
  // // Unmap input_img_buf
  // clEnqueueUnmapMemObject(queue, ocl_bufIn, input_img_map_buf, 0, NULL, NULL);
  // printf("\nMapping/Unmapping input_img_map_buf complete!\n");
  /******************************************************************/

  /********* Map fmask_buf into host address space **********/ 
  // fmask_map_buf = (int8_t *)clEnqueueMapBuffer(queue, ocl_bufMask, CL_TRUE, CL_MAP_WRITE /*CL_MAP_READ*/, 0, fmElements*sizeof(fmask_buf), 0, NULL, NULL, &status);
  // checkError(status, "Failed to map fmask_map_buf buffer");
  // // Write mask data into host buffer
  // for(int i=0;i<MASK_DIM;i++){
  //   for(int j=0;j<MASK_DIM;j++){
  //     fmask_map_buf[i*MASK_DIM + j] = fmask_buf[i][j];  
  //   }
  // }
  // // Unmap fmask_buf
  // clEnqueueUnmapMemObject(queue, ocl_bufMask, fmask_map_buf, 0, NULL, NULL);
  // printf("Mapping/Unmapping fmask_map_buf complete!\n");
  /******************************************************************/

  /********* Map ocl_output_buf into host address space ************/
  // ocl_output_map_buf = (uint8_t *)clEnqueueMapBuffer(queue, ocl_bufOut, CL_TRUE, /*CL_MAP_WRITE*/ CL_MAP_READ, 0, imBytes*sizeof(ocl_output_buf), 0, NULL, NULL, &status);
  // checkError(status, "Failed to map ocl_output_map_buf buffer");
  // // This process is continued in main() after the kernel is executed
  // printf("Mapping ocl_output_map_buf complete!\n");
  /******************************************************************/  

  /********* Create and EnqueueWrite Buffers ************/
  ocl_bufIn = clCreateBuffer(context,CL_MEM_READ_WRITE,imBytes*sizeof(input_img_buf[0]),0,&status);
  ocl_bufMask = clCreateBuffer(context,CL_MEM_READ_WRITE,imBytes*sizeof(fmask_buf[0]),0,&status);
  ocl_bufOut = clCreateBuffer(context,CL_MEM_READ_WRITE,imBytes*sizeof(ocl_output_buf[0]),0,&status);
  if(!ocl_bufIn || !ocl_bufMask || !ocl_bufOut){
    fprintf(stderr, "%s\n", "Failed to create buffers.");
  }

  clEnqueueWriteBuffer(queue,ocl_bufIn,0,0,sizeof(input_img_buf),input_img_buf,0,0,0);
  clEnqueueWriteBuffer(queue,ocl_bufMask,0,0,sizeof(fmask_buf),fmask_buf,0,0,0);
  clEnqueueWriteBuffer(queue,ocl_bufOut,0,0,sizeof(ocl_output_buf),ocl_output_buf,0,0,0);
  
  /***************** Create the kernel ****************************/
  // name passed in here must match kernel name in the
  // original CL file, that was compiled into an AOCX file using the AOC tool
  const char *kernel_name = "imfilter";  // Kernel name, as defined in the CL file
  kernel = clCreateKernel(program, kernel_name, &status);
  checkError(status, "Failed to create kernel");  

  return true;
}

int readKernelSource(const char* filename){
  FILE* fp = NULL;

  fp = fopen(filename, "rb");
  if(fp == 0) 
  {       
      return 0;
  }
  // get the length of the source code
  fseek(fp, 0, SEEK_END); 
  kernel_str_size = ftell(fp);
  fseek(fp, 0, SEEK_SET); 

  kernel_str = (char *)malloc(kernel_str_size+1); // +1? For \0 below

  int err = fread(kernel_str, kernel_str_size, 1, fp);
  if(err != 1){
    printf("\nFailed to read %s\n\n",filename);
    fclose(fp);
    free(kernel_str);
    return 0;
  }
  fclose(fp);
  return 1;
}

/*************************************************************************
 ************************ BUFFER INITIALIZATION **************************
 *************************************************************************/
void initBuffer(uint8_t* buf,uint32_t buf_size,const uint8_t buf_val){
  for(uint32_t i=0;i<buf_size;i++)
    buf[i] = buf_val;
}

static uint8_t ones(size_t size){
  uint8_t * temp;
  temp = new uint8_t[size];
  //temp = (uint8_t *)malloc(size * sizeof(temp));

  for (int i=0;i<size;i++)
    temp[i] = 1;

  return *temp;
}

/*************************************************************************
 **************************** CPU ALGORITHM ******************************
 *************************************************************************/
// Execute naive convolution using CPU (C/C++)
void cpuConvolution(){
  // TODO:  verify that this works; the results are strange, but it could
  //        be the PNG encode function
  ////////////// DEBUG ///////////////
  //memcpy(cpu_output_buf,input_img_buf,imBytes*sizeof(uint8_t));
  ///////////////////////////////////

  int pitch = (int)PITCH;

  for(int i = 0; i < imHeight*4; i++){
    for(int k = 0; k < imWidth*4; k++){
   
      // Index of pixel being convolved
      int img_ind = i*imWidth + k;
          
      uint8_t sum = 0;

      // Loop through the mask
      for(int l = - pitch; l < MASK_DIM-pitch; l++){
        for(int m = -pitch; m < MASK_DIM-pitch; m++){

          // Check bounds
          if((i + l <= imHeight*4-pitch && i >= pitch) && (k+m <= imWidth*4-pitch && k >= pitch))
            sum += fmask_buf[l+pitch][m+pitch] * input_img_buf[img_ind+(imWidth*4)*l+m];
        }
      }
      
        int res_ind = img_ind + (imWidth * pitch) + pitch;
        if((i < imWidth*4-pitch && i >= pitch) && (k < imHeight*4-pitch && k>= pitch))
          cpu_output_buf[img_ind] = sum;
        else
         cpu_output_buf[img_ind] = input_img_buf[img_ind];
    }
  }
}

/*************************************************************************
 ************************** PNG DECODE/ENCODE ****************************
 *************************************************************************/
void decodePNG(const char* filename)
{
  //unsigned error = lodepng_decode32_file(&input_img_buf, &imWidth, &imHeight, filename); // Changed to temp_img_buf
  unsigned error = lodepng_decode32_file(&temp_img_buf, &imWidth, &imHeight, filename); 
  if(error) 
    printf("error %u: %s\n", error, lodepng_error_text(error));

  printf("Decoded Image Attributes: \n");
  printf("=========================\n");
  printf("%-40s = %s\n", "FILE NAME", filename);
  printf("%-40s = %d\n", "IMAGE WIDTH", imWidth);
  printf("%-40s = %d\n\n", "IMAGE HEIGHT", imHeight);
}

void encodePNG(const char* filename, const unsigned char* image, unsigned imWidth, unsigned imHeight)
{
  /*Encode the image*/
  unsigned error = lodepng_encode32_file(filename, image, imWidth, imHeight);
  if(error) 
    printf("error %u: %s\n", error, lodepng_error_text(error));


  printf("Encoded Image Attributes:\n");
  printf("=========================\n");
  printf("%-40s = %s\n", "FILE NAME", filename);
  printf("%-40s = %d\n", "IMAGE WIDTH", imWidth);
  printf("%-40s = %d\n\n", "IMAGE HEIGHT", imHeight);
}
/*************************************************************************/

/*************************************************************************
 ************************** FREE UP RESOURCES ****************************
 *************************************************************************/
void cleanup() {
  if(kernel) {
    clReleaseKernel(kernel);  
  }
  if(program) {
    clReleaseProgram(program);
  }
  if(queue) {
    clReleaseCommandQueue(queue);
  }
  if(context) {
    clReleaseContext(context);
  }
  if(ocl_bufIn){
    clReleaseMemObject(ocl_bufIn);
  }
  if(ocl_bufMask){
    clReleaseMemObject(ocl_bufMask);
  }
  if(ocl_bufOut){
    clReleaseMemObject(ocl_bufOut);
  }
  if(input_img_buf){
    free(input_img_buf);
  }
  if(temp_img_buf){
    free(temp_img_buf);
  }
  if(ocl_output_buf){
    free(ocl_output_buf);
  }
  if(cpu_output_buf){
    free(cpu_output_buf);
  }
}

/*************************************************************************
 ************************ OPENCL DISPLAY PARAMS **************************
 *************************************************************************/
static void device_info_ulong( cl_device_id device, cl_device_info param, const char* name) {
   cl_ulong a;
   clGetDeviceInfo(device, param, sizeof(cl_ulong), &a, NULL);
   printf("%-40s = %lu\n", name, a);
}
static void device_info_uint( cl_device_id device, cl_device_info param, const char* name) {
   cl_uint a;
   clGetDeviceInfo(device, param, sizeof(cl_uint), &a, NULL);
   printf("%-40s = %u\n", name, a);
}
static void device_info_bool( cl_device_id device, cl_device_info param, const char* name) {
   cl_bool a;
   clGetDeviceInfo(device, param, sizeof(cl_bool), &a, NULL);
   printf("%-40s = %s\n", name, (a?"true":"false"));
}
static void device_info_string( cl_device_id device, cl_device_info param, const char* name) {
   char a[STRING_BUFFER_LEN]; 
   clGetDeviceInfo(device, param, STRING_BUFFER_LEN, &a, NULL);
   printf("%-40s = %s\n", name, a);
}

static void display_device_info( cl_device_id device ) {

   printf("Querying device for info:\n");
   printf("========================\n");
   device_info_string(device, CL_DEVICE_NAME, "CL_DEVICE_NAME");
   device_info_string(device, CL_DEVICE_VENDOR, "CL_DEVICE_VENDOR");
   device_info_uint(device, CL_DEVICE_VENDOR_ID, "CL_DEVICE_VENDOR_ID");
   device_info_string(device, CL_DEVICE_VERSION, "CL_DEVICE_VERSION");
   device_info_string(device, CL_DRIVER_VERSION, "CL_DRIVER_VERSION");
   device_info_uint(device, CL_DEVICE_ADDRESS_BITS, "CL_DEVICE_ADDRESS_BITS");
   device_info_bool(device, CL_DEVICE_AVAILABLE, "CL_DEVICE_AVAILABLE");
   device_info_bool(device, CL_DEVICE_ENDIAN_LITTLE, "CL_DEVICE_ENDIAN_LITTLE");
   device_info_ulong(device, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, "CL_DEVICE_GLOBAL_MEM_CACHE_SIZE");
   device_info_ulong(device, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, "CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE");
   device_info_ulong(device, CL_DEVICE_GLOBAL_MEM_SIZE, "CL_DEVICE_GLOBAL_MEM_SIZE");
   device_info_bool(device, CL_DEVICE_IMAGE_SUPPORT, "CL_DEVICE_IMAGE_SUPPORT");
   device_info_ulong(device, CL_DEVICE_LOCAL_MEM_SIZE, "CL_DEVICE_LOCAL_MEM_SIZE");
   device_info_ulong(device, CL_DEVICE_MAX_CLOCK_FREQUENCY, "CL_DEVICE_MAX_CLOCK_FREQUENCY");
   device_info_ulong(device, CL_DEVICE_MAX_COMPUTE_UNITS, "CL_DEVICE_MAX_COMPUTE_UNITS");
   device_info_ulong(device, CL_DEVICE_MAX_CONSTANT_ARGS, "CL_DEVICE_MAX_CONSTANT_ARGS");
   device_info_ulong(device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, "CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE");
   device_info_uint(device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS");
   device_info_uint(device, CL_DEVICE_MEM_BASE_ADDR_ALIGN, "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS");
   device_info_uint(device, CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE, "CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT");
   device_info_uint(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, "CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE");

   {
      cl_command_queue_properties ccp;
      clGetDeviceInfo(device, CL_DEVICE_QUEUE_PROPERTIES, sizeof(cl_command_queue_properties), &ccp, NULL);
      printf("%-40s = %s\n", "Command queue out of order? ", ((ccp & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)?"true":"false"));
      printf("%-40s = %s\n", "Command queue profiling enabled? ", ((ccp & CL_QUEUE_PROFILING_ENABLE)?"true":"false"));
   }
}